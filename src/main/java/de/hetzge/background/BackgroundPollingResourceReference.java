package de.hetzge.background;

import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.request.resource.ResourceReference;

public class BackgroundPollingResourceReference extends ResourceReference {
	public BackgroundPollingResourceReference() {
		super("background");
	}

	@Override
	public IResource getResource() {
		return new BackgroundPollingResource();
	}
}
