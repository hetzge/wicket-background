package de.hetzge.background;

import java.io.Serializable;

final class BackgroundValueFuture implements Serializable {

	private Object value = null;
	private Exception exception = null;
	private boolean done = false;

	public synchronized void setValue(Object value) {
		if (!this.done) {
			this.value = value;
			this.done = true;
		} else {
			throw new IllegalStateException("Value is already set. Value can only be set once.");
		}
	}

	public synchronized Object getValue() {
		if (this.done) {
			return this.value;
		} else {
			throw new IllegalStateException("Accessing value before it is done is not possible.");
		}
	}

	public synchronized void setException(Exception exception) {
		if (!this.done) {
			this.exception = exception;
			this.done = true;
		} else {
			throw new IllegalStateException("Value is already set. Value can only be set once.");
		}
	}

	public synchronized Exception getException() {
		return this.exception;
	}

	public boolean isDone() {
		return this.done;
	}
}
