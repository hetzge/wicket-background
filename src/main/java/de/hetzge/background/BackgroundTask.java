package de.hetzge.background;

import java.io.Serializable;

import org.apache.wicket.core.request.handler.IPartialPageRequestHandler;

/**
 * If you want do some execution in the background implement this interface and
 * pass it to {@link Background#run(org.apache.wicket.Page, BackgroundTask)}.
 * 
 * @param <T>
 *            the type of the result of the calculation
 */
public interface BackgroundTask<T> extends Serializable {

	/**
	 * Do the background calculation in this method. It will be executed in a
	 * other thread. Be aware of this and avoid dependencies to wicket component
	 * instances.
	 * 
	 * @return The calculated value.
	 * @throws Exception
	 *             If something went wrong. You can handle the exception in the
	 *             {@link #onError(IPartialPageRequestHandler, Exception)}
	 *             method.
	 */
	T calculate() throws Exception;

	/**
	 * Override this method to update the ui if the calculation completed with
	 * success.
	 * 
	 * @param target
	 * @param result
	 *            in {@link #calculate()} calculated value
	 */
	void onComplete(IPartialPageRequestHandler target, T result);

	/**
	 * You can optionally override this method to react on exceptions which
	 * happened while {@link #calculate()}.
	 * 
	 * @param target
	 * @param exception
	 *            the exception which happened
	 */
	default void onError(IPartialPageRequestHandler target, Exception exception) {
	}

	/**
	 * Not part of the public api!
	 */
	@SuppressWarnings("unchecked")
	default void onComplete0(IPartialPageRequestHandler target, Object result) {
		onComplete(target, (T) result);
	}
}