package de.hetzge.background;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import javax.servlet.AsyncContext;

import org.apache.wicket.protocol.http.servlet.ServletWebRequest;
import org.apache.wicket.request.resource.AbstractResource;

// https://stackoverflow.com/questions/12826540/java-long-polling-separate-thread
public final class BackgroundPollingResource extends AbstractResource {

	@Override
	protected ResourceResponse newResourceResponse(Attributes attributes) {
		final ServletWebRequest servletWebRequest = (ServletWebRequest) attributes.getRequest();
		final AsyncContext asyncContext = servletWebRequest.getContainerRequest().startAsync();

		final String pageUUID = servletWebRequest.getRequestParameters().getParameterValue("pageUUID").toString();

		final long startTimestamp = System.currentTimeMillis();

		boolean valuesAvailable;
		pollLoop: while (true) {

			valuesAvailable = poll(pageUUID);

			if (valuesAvailable) {
				break pollLoop;
			}

			if (System.currentTimeMillis() - startTimestamp > Background.LONG_POLLING_TIMEOUT.toMillis()) {
				break pollLoop;
			}

			try {
				Thread.sleep(500);
			} catch (final InterruptedException e) {
				break pollLoop;
			}
		}

		final String result = String.valueOf(valuesAvailable);

		final ResourceResponse resourceResponse = new ResourceResponse();
		resourceResponse.setTextEncoding("utf-8");
		resourceResponse.setContentType("application/json");
		resourceResponse.getHeaders().addHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
		resourceResponse.setWriteCallback(new WriteCallback() {
			@Override
			public void writeData(Attributes attributes) throws IOException {

				final OutputStream outputStream = attributes.getResponse().getOutputStream();
				try (OutputStreamWriter writer = new OutputStreamWriter(outputStream)) {
					writer.append(result);
				}

				asyncContext.complete();
			}
		});

		return resourceResponse;
	}

	private boolean poll(String pageUUID) {
		final BackgroundValues backgroundValues = BackgroundSession.getValues();
		Background.keepAlive(pageUUID);
		return backgroundValues.poll(pageUUID);
	}

}
