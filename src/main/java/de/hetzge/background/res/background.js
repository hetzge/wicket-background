function startBackground(config) {
	var request;
	(function poll(){
		request = $.ajax({
			method: 'GET',
			cache: false,
			url: '/background/poll/',
			data: {
				pageUUID : config.pageUUID
			},
			success: function(data) {
				if (data === 'true') {
					config.callback();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log('wicket background polling error', jqXHR, textStatus, errorThrown);
			},
			timeout: config.timeout,
			dataType: 'text',
			complete: poll
		});
	}());
	window.addEventListener('beforeunload', function(event) {
		if(request !== undefined){
			request.abort();
		}
    });
}