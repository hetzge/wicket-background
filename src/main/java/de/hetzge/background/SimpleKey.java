package de.hetzge.background;

import java.util.UUID;

import org.apache.wicket.Page;

public final class SimpleKey implements Key {

	private final String pageUUID;
	private final String uuid;

	public SimpleKey(Page page) {
		this.pageUUID = BackgroundBehavior.getFor(page).getPageUUID();
		this.uuid = UUID.randomUUID().toString();
	}

	@Override
	public String getPageUUID() {
		return this.pageUUID;
	}

	@Override
	public String getUUID() {
		return this.uuid;
	}

	@Override
	public String toString() {
		return "BackgroundValueKey [pageUUID=" + this.pageUUID + ", uuid=" + this.uuid + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.uuid == null) ? 0 : this.uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final SimpleKey other = (SimpleKey) obj;
		if (this.uuid == null) {
			if (other.uuid != null) {
				return false;
			}
		} else if (!this.uuid.equals(other.uuid)) {
			return false;
		}
		return true;
	}

}
