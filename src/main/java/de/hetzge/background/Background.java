package de.hetzge.background;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.wicket.Application;
import org.apache.wicket.Page;
import org.apache.wicket.Session;
import org.apache.wicket.ThreadContext;
import org.apache.wicket.protocol.http.WebApplication;

public final class Background {

	static {
		new SupervisorThread().start();
	}

	public static final Duration LONG_POLLING_TIMEOUT = Duration.ofSeconds(30);
	public static final Duration BACKGROUND_TASK_ALIVE_TIMEOUT = Duration.ofSeconds(3);

	private static final Map<Key, RunningBackgroundTask> runningBackgroundTasksByKey = new ConcurrentHashMap<>();
	private static final ExecutorService EXECUTOR_SERVICE = Executors.newCachedThreadPool();

	private Background() {
	}

	public static void init(WebApplication application) {
		application.mountResource("/background/poll", new BackgroundPollingResourceReference());
	}

	/**
	 * Runs a given task as background task.
	 * 
	 * @param page
	 *            the current page the background task should run for
	 * @param backgroundTask
	 *            the task which should be started in the background
	 */
	public static void run(Page page, BackgroundTask<?> backgroundTask) {

		final Key key = new SimpleKey(page);

		if (Session.get().getId() == null) {
			throw new IllegalStateException("Can't run background prozess if no session is active.");
		}

		final BackgroundBehavior backgroundBehavior = BackgroundBehavior.getFor(page);
		backgroundBehavior.register(key, backgroundTask);

		final BackgroundValues backgroundValues = BackgroundSession.getValues();
		final BackgroundValueFuture backgroundValueFuture = backgroundValues.register(key);

		final Session session = Session.get();
		final Application application = Application.get();

		final Future<?> future = EXECUTOR_SERVICE.submit(() -> {
			try {
				ThreadContext.setSession(session);
				ThreadContext.setApplication(application);

				final Object result = backgroundTask.calculate();
				backgroundValueFuture.setValue(result);
				runningBackgroundTasksByKey.remove(key);
			} catch (final InterruptedException ex) {
				System.out.println(String.format("Interruption of background task: %s", ex.getMessage()));
			} catch (final Exception ex) {
				backgroundValueFuture.setException(ex);
			}
		});

		runningBackgroundTasksByKey.put(key, new RunningBackgroundTask(future));
	}

	/**
	 * Marks all running background tasks with a specific page uuid as alive.
	 */
	public static void keepAlive(String pageUUID) {
		runningBackgroundTasksByKey.forEach((key, runningBackgroundTask) -> {
			if (key.getPageUUID().equals(pageUUID)) {
				runningBackgroundTask.keepAlive();
			}
		});
	}

	/**
	 * Watches running background tasks periodically. If they are not keeped
	 * alive for some time they will be stopped and cleaned up. Prevents long
	 * running tasks from running endless if the matching page already closed.
	 */
	private static class SupervisorThread extends Thread {

		public SupervisorThread() {
			super("Wicket Background Supervisor");
		}

		@Override
		public void run() {
			loop: while (true) {
				runningBackgroundTasksByKey.forEach((key, value) -> {
					if (value.hasTimedOut()) {
						kill(key);
					}
				});

				try {
					// sleep to reduce load
					Thread.sleep(1000);
				} catch (final InterruptedException e) {
					break loop;
				}
			}
		}

		private static void kill(Key key) {
			System.out.println(String.format("!!! ### !!! KILL %s !!! ### !!!", key));
			runningBackgroundTasksByKey.get(key).stop();
			runningBackgroundTasksByKey.remove(key);
		}
	}

}
