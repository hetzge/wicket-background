package de.hetzge.background;

import java.io.Serializable;

interface Key extends Serializable {
	String getPageUUID();

	String getUUID();
}
