package de.hetzge.background;

import java.io.Serializable;

import org.apache.wicket.Session;

/**
 * Util class which allowes to access the {@link BackgroundValues} which are
 * stored in the session.
 */
final class BackgroundSession {
	private static final String BACKGROUND_VALUES_SESSION_ATTRIBUTE = "BACKGROUND_VALUES";

	public static BackgroundValues getValues() {
		final Session session = Session.get();
		final Serializable backgroundValues = session.getAttribute(BACKGROUND_VALUES_SESSION_ATTRIBUTE);

		if (backgroundValues != null) {
			return (BackgroundValues) backgroundValues;
		} else {
			session.setAttribute(BACKGROUND_VALUES_SESSION_ATTRIBUTE, new BackgroundValues());
			return getValues();
		}
	}

	private BackgroundSession() {
	}
}
