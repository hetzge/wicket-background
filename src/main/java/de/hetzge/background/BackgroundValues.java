package de.hetzge.background;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

final class BackgroundValues implements Serializable {

	private final Map<Key, BackgroundValueFuture> backgroundValuesByKey = new ConcurrentHashMap<>();
	private final Map<Key, BackgroundValueFuture> valuesByKey = new ConcurrentHashMap<>();

	public BackgroundValueFuture register(Key key) {
		final BackgroundValueFuture backgroundValueFuture = new BackgroundValueFuture();
		this.backgroundValuesByKey.put(key, backgroundValueFuture);
		return backgroundValueFuture;
	}

	public synchronized boolean poll(String pageUUID) {

		boolean done = false;

		for (final Entry<Key, BackgroundValueFuture> entry : this.backgroundValuesByKey.entrySet()) {
			if (entry.getKey().getPageUUID().equals(pageUUID) && entry.getValue().isDone()) {
				done = true;
				this.valuesByKey.put(entry.getKey(), entry.getValue());
				this.backgroundValuesByKey.remove(entry.getKey());
			}
		}

		return done;
	}

	public synchronized Map<Key, BackgroundValueFuture> flush(String pageUUID) {

		final Map<Key, BackgroundValueFuture> result = new HashMap<>();

		for (final Entry<Key, BackgroundValueFuture> entry : this.valuesByKey.entrySet()) {
			final Key key = entry.getKey();
			if (key.getPageUUID().equals(pageUUID)) {
				result.put(key, entry.getValue());
				this.valuesByKey.remove(key);
			}
		}

		return result;
	}
}
