package de.hetzge.background;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.wicket.Component;
import org.apache.wicket.Page;
import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptContentHeaderItem;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

public final class BackgroundBehavior extends AbstractDefaultAjaxBehavior {

	private TasksByKey tasksByKey;
	private TasksByKey nextTasksByKey;

	private String nextUUID;
	private String pageUUID;

	public BackgroundBehavior() {
		this.nextUUID = UUID.randomUUID().toString();
		this.pageUUID = this.nextUUID;

		this.nextTasksByKey = new TasksByKey();
		this.tasksByKey = this.nextTasksByKey;
	}

	@Override
	public void renderHead(Component component, IHeaderResponse response) {
		super.renderHead(component, response);

		final CharSequence callbackFunction = getCallbackFunction();

		this.pageUUID = this.nextUUID;
		this.tasksByKey = this.nextTasksByKey;
		this.nextUUID = UUID.randomUUID().toString();
		this.nextTasksByKey = new TasksByKey();
		System.out.println("switched pageUUID to " + this.pageUUID);

		response.render(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(BackgroundBehavior.class, "res/background.js")));
		response.render(JavaScriptContentHeaderItem.forScript(String.format("startBackground({ callback: %s, pageUUID: '%s', timeout: %s })", callbackFunction, this.pageUUID, Background.LONG_POLLING_TIMEOUT.toMillis() + 5000), "wicket-background"));
	}

	@Override
	protected void respond(AjaxRequestTarget target) {
		final BackgroundValues backgroundValues = BackgroundSession.getValues();

		backgroundValues.flush(this.pageUUID).entrySet().stream().forEach(entry -> {
			final Key key = entry.getKey();
			final BackgroundValueFuture doneBackgroundValueFuture = entry.getValue();
			final Object value = doneBackgroundValueFuture.getValue();
			final Exception exception = doneBackgroundValueFuture.getException();

			if (exception == null) {
				try {
					this.tasksByKey.get(key).onComplete0(target, value);
				} catch (final NullPointerException ex) {
					System.out.println(String.format("NullPointerException at key %s", key));
					this.tasksByKey.log();
					ex.printStackTrace();
				}
			} else {
				this.tasksByKey.get(key).onError(target, exception);
			}

			this.tasksByKey.remove(key);
		});
	}

	public String getPageUUID() {
		return this.pageUUID;
	}

	void register(Key key, BackgroundTask<?> task) {
		this.tasksByKey.add(key, task);
	}

	public static BackgroundBehavior getFor(Page page) {
		final List<BackgroundBehavior> behaviors = page.getBehaviors(BackgroundBehavior.class);
		if (behaviors.isEmpty()) {
			page.add(new BackgroundBehavior());
			page.add(new AjaxEventBehavior("beforeunload") {

				@Override
				protected void onEvent(AjaxRequestTarget target) {
					// TODO kill background processes
				}
			});
			return getFor(page);
		} else if (behaviors.size() == 1) {
			return behaviors.get(0);
		} else {
			throw new IllegalStateException("BackgoundBehavior is added twice");
		}
	}

	private class TasksByKey implements Serializable {
		private Map<Key, BackgroundTask<?>> tasksByKey = new LinkedHashMap<>();

		public synchronized BackgroundTask<?> get(Key key) {
			return this.tasksByKey().get(key);
		}

		public synchronized void add(Key key, BackgroundTask<?> task) {
			System.out.println(String.format("Add task: %s / %s / %s", System.identityHashCode(this), BackgroundBehavior.this.pageUUID, key));
			this.tasksByKey.put(key, task);
		}

		public synchronized void remove(Key key) {
			System.out.println(String.format("Remove task: %s / %s / %s", System.identityHashCode(this), BackgroundBehavior.this.pageUUID, key));
			this.tasksByKey().remove(key);
		}

		private Map<Key, BackgroundTask<?>> tasksByKey() {
			if (this.tasksByKey == null) {
				this.tasksByKey = new HashMap<>();
			}
			return this.tasksByKey;
		}

		public void log() {
			System.out.println("Identity: " + System.identityHashCode(this.tasksByKey));
			System.out.println("available: " + BackgroundBehavior.this.pageUUID + " -> " + this.tasksByKey.keySet().stream().map(Key::getUUID).collect(Collectors.joining(", ")));
		}
	}
}
