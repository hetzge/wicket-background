package de.hetzge.background;

import java.util.concurrent.Future;

final class RunningBackgroundTask {

	private final Future<?> future;
	private long aliveTimestamp;

	public RunningBackgroundTask(Future<?> future) {
		this.future = future;
		this.aliveTimestamp = System.currentTimeMillis();
	}

	public void keepAlive() {
		this.aliveTimestamp = System.currentTimeMillis();
	}

	public boolean hasTimedOut() {
		return System.currentTimeMillis() - this.aliveTimestamp > Background.BACKGROUND_TASK_ALIVE_TIMEOUT.toMillis();
	}

	public void stop() {
		this.future.cancel(true);
	}
}