package de.hetzge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.wicket.core.request.handler.IPartialPageRequestHandler;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import de.hetzge.background.Background;
import de.hetzge.background.BackgroundTask;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;

	public HomePage(final PageParameters parameters) {
		super(parameters);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();

		getSession().bind();

		add(new Label("session", getSession().getId()));
		add(new BookmarkablePageLink<>("link", HomePage.class));

		final IModel<List<String>> messagesModel = Model.ofList(new ArrayList<String>());

		final WebMarkupContainer messagesContainer = new WebMarkupContainer("messagesContainer");
		messagesContainer.setOutputMarkupId(true);
		add(messagesContainer);
		messagesContainer.add(new ListView<String>("messages", messagesModel) {
			@Override
			protected void populateItem(ListItem<String> item) {
				item.add(new Label("message", item.getModel()));
			}
		});

		for (int i = 0; i < 50; i++) {
			final int x = i;
			Background.run(getPage(), new BackgroundTask<List<String>>() {

				@Override
				public List<String> calculate() throws Exception {
					Thread.sleep(5000 + (x * 500));
					System.out.println("Done");

					return Arrays.asList("" + Math.random() + "...");
				}

				@Override
				public void onComplete(IPartialPageRequestHandler target, List<String> result) {
					System.out.println("Complete");
					messagesModel.setObject(result);
					target.add(messagesContainer);
				}

				@Override
				public void onError(IPartialPageRequestHandler target, Exception exception) {
					System.out.println("Error");
					exception.printStackTrace();
				}
			});
		}
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);

		response.render(JavaScriptHeaderItem.forReference(getApplication().getJavaScriptLibrarySettings().getJQueryReference()));
	}

}
